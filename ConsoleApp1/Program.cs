﻿using System;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Data;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int loginAttempts = 0;
            var pass = string.Empty;
            ConsoleKey key;


            //With our INT we give the user a max attempt of 3 attempts to login in,
            //IF if the user fails to login 3 times it will say Your Credentials are incorrect
            //ELSE -- if the user entered the credentianls succesfully in its 3 attempts it will output Your credentials are correct,
            //the \n stands for a new line so we dont have to use a empty console command.
            Console.WriteLine("____________________________");
            Console.WriteLine("|----Welkom please login----|");
            Console.WriteLine("----------------------------");
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Enter username:");
                string username = Console.ReadLine();
                Console.WriteLine("Enter Password:");
                do
                {

                    var keyInfo = Console.ReadKey(intercept: true);
                    key = keyInfo.Key;

                    if (key == ConsoleKey.Backspace && pass.Length > 5)
                    {
                        Console.Write("\b \b");
                        pass = pass[0..^1];
                    }
                    else if (!char.IsControl(keyInfo.KeyChar))
                    {
                        Console.Write("*");
                        pass += keyInfo.KeyChar;
                    }
                } while (key != ConsoleKey.Enter);

                if (username != "user" || pass != "valid")
                    loginAttempts++;
                else
                    break;
            }

            //just like with the login we give the user attempts to type the correct command for X
            //amount of time in this case we up it by 2 so instead of 3 attempts we give the user 5 atempts after that we going to give it 2 seconds to close the console
            if (loginAttempts > 2)
            {
                Console.WriteLine("\nYour Credentials are incorrect. The program will now close.");
                Thread.Sleep(2000);
                Environment.Exit(1);
            }
            else
            {
                Console.WriteLine("\nYour credentials are correct\n");
                Console.WriteLine("Welcome,to see your current Commands type 'Commands' and hit ENTER. ");
                int CommandAttempts = 0;
                for (int i = 0; i < 5; i++)
                {

                    string command = Console.ReadLine();

                    if (command != "Commands")
                        CommandAttempts++;
                    else
                        break;
                }
                if (CommandAttempts > 2)
                {
                    Console.WriteLine("You exceeded the total attempts the program will now close.");
                    Thread.Sleep(2000);
                    Environment.Exit(1);
                }
                else
                {
                    Console.WriteLine("____________________________");
                    Console.WriteLine("|-------Command list-------|");
                    Console.WriteLine("----------------------------");

                    ///Main program. This should be where you run your console app, but keep as much as you can in other functions.

                    bool quit = false;

                    while (quit == false) // Could also use !quit
                    {
                        Console.WriteLine("\n--Select an option--");
                        Console.WriteLine("A: Print all inventory");
                        Console.WriteLine("B: Edit Inventory");
                        Console.WriteLine("");
                        Console.WriteLine("Type 'Exit' to quit the program\n"); //If you do not have this, the while loop will never end.

                        //Could also move the print statements to a function and call that function

                        string option = Console.ReadLine(); //Read in the user's option. I only defined A, kind of.

                        switch (option) //Can use one switch statement, or a series of connected if statements
                        {
                            case "A":
                                CommandA(); //Call the code to print the inventory.
                                break;
                            case "B":
                                CommandB(); //Call the code to print brands
                                break;
                            default:
                                Console.WriteLine("Option does not exist. Please enter a valid option.");
                                break;
                            case "Exit":
                                quit = true;
                                break;
                        }
                    }

                    void CommandA()
                    {
                        string NikeID = "Nike1.csv";
                        string searchText = File.ReadAllText(NikeID);
                        Console.WriteLine("____________________________");
                        Console.WriteLine("|------Local Inventory-------|");
                        Console.WriteLine("----------------------------");
                        Console.Write(searchText);
                    }

                    void CommandB()
                    {
                        string NikeID = "Nike1.csv";
                        string searchText = File.ReadAllText(NikeID);

                        Console.WriteLine("___________________________");
                        Console.WriteLine("|------Edit Inventory ------|");
                        Console.WriteLine("---------------------------");
                        Console.Write(searchText);

                        Console.WriteLine("");

                        //giving another command for the user to select a item.
                        Console.WriteLine("What would you like to do:");
                        Console.WriteLine("A-Show Nike:");
                        Console.WriteLine("B-Show Adidas:");

                        string Command1 = Console.ReadLine();

                        if (Command1 == "A")
                        {
                            
                        }
                        else
                        {
                            if (Command1 == "B")
                            {
                                Console.Write("Current Name:"); Console.Write(searchText);
                                Console.WriteLine("");
                                Console.Write("New Name:");
                                string replaceText = Console.ReadLine();
                                ReplaceInFile(NikeID, searchText, replaceText);
                            }
                            else
                            {
                                Console.Write("Incorrect Command:");
                            }
                        }
                    }
                    static void ReplaceInFile(string filePath, string searchText, string replaceText)
                    {
                        StreamReader reader = new StreamReader(filePath);
                        string content = reader.ReadToEnd();
                        reader.Close();
                        content = Regex.Replace(content, searchText, replaceText);
                        StreamWriter writer = new StreamWriter(filePath);
                        writer.Write(content);
                        writer.Close();
                    }
                }
            }
        }
    }
}